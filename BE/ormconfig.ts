import {env} from '~/types/env';

export default {
  "type": "mysql",
  "host": env.DB_HOST,
  "port": 3306,
  "username": env.DB_USER,
  "password": env.DB_PASS,
  "database": env.DB_NAME,
  "synchronize": true,
  "logging": false,
  cache: {
    duration: env.NODE_ENV === 'development' ? 0 : 5
  },
  "entities": [
    __dirname + "/src/entity/*.ts"
  ],
  "migrations": [
    "src/migration/*.ts"
  ]
};
