import express from "express";
const router = express.Router();
import {Request, Response} from "express";
import type {Exception} from "easy-visa-api/exceptions/Exception";
import FilesFacade from "~/facades/FilesFacade";
import path from "path";
import {result} from "lodash";

router.get(`/`, async (req: Request, res: Response) => {
  try {
    res.sendFile(path.resolve(FilesFacade.getFilePath(req.query?.type.toString())));
  } catch (e: ReturnType<Exception>) {
    res.statusCode = 400;
    console.log(e);
    res.send(result(e, 'stringify', e.toString()));
  }
});

export default router;
