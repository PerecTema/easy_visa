import express from "express";

const router = express.Router();
import {Request, Response} from "express";
import NewUserDto from "easy-visa-api/dto/NewUserDto";
import UserDataDto from "easy-visa-api/dto/UserDataDto";
import {Exception} from "easy-visa-api/exceptions/Exception";
import {RoutUserEnums} from "easy-visa-api/Enums/RoutEnums";
import UserFacade from "~/facades/UserFacade";
import UserDataFacade from "~/facades/UserDataFacade";
import {get, result} from "lodash";

const userFacade = new UserFacade();
const userDataFacade = new UserDataFacade();

router.post(`/${RoutUserEnums.userCreate}`, async (req: Request, res: Response) => {
    try {
        const body = req.body as NewUserDto;
        const user = await userFacade.createUser(body);
        delete user.confirmEmailHash;
        res.send(user);
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 400;
        console.log(e);
        res.send(result(e, 'stringify', e.toString()));
    }
});

router.put(`/${RoutUserEnums.putUserData}`, async (req: Request, res: Response) => {
    try {
        const body = req.body as UserDataDto;
        let user = await userFacade.verifyToken(req.headers.authorization);
        if(req.query.id){
            user = (await userFacade.canUpdateOtherUser(user, req.query.id.toString())).otherUser;
        }
        const userData = await userDataFacade.updateUserData(user, body);
        res.send(userData);
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 400;
        console.log(e);
        res.send(result(e, 'stringify', e.toString()));
    }
});

router.get(`/${RoutUserEnums.getUserData}`, async (req: Request, res: Response) => {
    try {
        const user = await userFacade.verifyToken(req.headers.authorization);
        const userData = await userDataFacade.getUserDataByUserId(get(req, 'query.id', user.id), user);
        res.send(userData);
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 400;
        console.log(e);
        res.send(result(e, 'stringify', e.toString()));
    }
});

router.get(`/${RoutUserEnums.getUsersList}`, async (req: Request, res: Response) => {
    try {
        const user = await userFacade.verifyToken(req.headers.authorization);
        res.send(await userFacade.getUsers(user));
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 400;
        console.log(e);
        res.send(result(e, 'stringify', e.toString()));
    }
});

router.post(`/${RoutUserEnums.addUser}`, async (req: Request, res: Response) => {
    try {
        const user = await userFacade.verifyToken(req.headers.authorization);
        const body = req.body as NewUserDto;
        body.tenant = user.tenant;
        res.send(await userFacade.addClient(user, body));
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 400;
        console.log(e);
        res.send(result(e, 'stringify', e.toString()));
    }
});

router.post(`/${RoutUserEnums.userLogin}/:confirmEmailHash?`, async (req: Request, res: Response) => {
    try {
        const body = req.body as NewUserDto;
        if (!body.email || !body.password) {
            res.statusCode = 401;
            res.send("nop!");
            // todo: add input validator
            return;
        }
        const user = await userFacade.loginUser(body, get(req.params, 'confirmEmailHash'));
        res.send(user);
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 401;
        console.log(e);
        res.send(result(e, 'stringify', e.toString()));
    }
});

router.get(`/${RoutUserEnums.userGet}`, async (req: Request, res: Response) => {
    try {
        res.send(await userFacade.getLoginUser(req.headers.authorization));
    } catch (e: ReturnType<Exception>) {
        res.statusCode = 400;
        res.send(result(e, 'stringify', e.toString()));
    }
});


export default router;
