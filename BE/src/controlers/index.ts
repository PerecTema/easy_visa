import userRouter from "~/controlers/UserControler";
import filesRouter from "~/controlers/FilesControler";
import {RoutUserEnums} from "easy-visa-api/Enums/RoutEnums";
import {RoutFilesEnums} from "easy-visa-api/Enums/RoutEnums";
import express from "express";
import UserFacade from "~/facades/UserFacade";

const userFacade = new UserFacade();

const router = express.Router();

router.use(async (req, res, next) => {
  const uuidRegex = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}';
  const isRoutPublic = [
    `/${RoutUserEnums.user}/${RoutUserEnums.userLogin}/?(${uuidRegex})?`,
    `/${RoutUserEnums.user}/${RoutUserEnums.userCreate}`,
  ].some(patern => new RegExp(patern).test(req.path));

  if (isRoutPublic) {
    next();
    return;
  }
  if (req.headers.authorization && !await userFacade.verifyToken(req.headers.authorization)) {
    res.status(401).send("Access token is not valid");
    return;
  }
  next();
});

router.use('/user', userRouter);
router.use(`/${RoutFilesEnums.files}`, filesRouter);

export default router;

