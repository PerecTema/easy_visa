import {EntityRepository, Not} from "typeorm";
import AbstractRepository from "~/repository/AbstractRepository";
import {User} from "~/entity/User";
import NewUserDto from "easy-visa-api/dto/NewUserDto";
import {DuplicateException} from "easy-visa-api/exceptions/DuplicateException";
import {NotFoundException} from "easy-visa-api/exceptions/NotFoundException";
import {ErrorCodes,} from "easy-visa-api/exceptions/ErrorCodes";
import {createHash} from "crypto";
import {v4 as uuid} from "uuid";

export const hash = (pass: string) => createHash('sha256').update(pass).digest('hex');

@EntityRepository(User)
export default class UserRepository extends AbstractRepository<User> {

  constructor() {
    super(User);
  }

  getUserByEmail(email: string): Promise<User> {
    return this.repository.findOne({where: {email}});
  }

  getUserByToken(token: string): Promise<User> {
    return this.repository.findOne({where: {token}});
  }

  async isEmailConfirmed(email: string): Promise<boolean> {
    return true;
    //return !(await this.repository.findOne({where: {email}, select: ['confirmEmailHash']})).confirmEmailHash;
  }

  async loginUser(user: NewUserDto): Promise<User> {
    const token = uuid();
    if ((await this.repository.update({email: user.email, password: hash(user.password)}, {token})).affected === 1) {
      const loggedUser = await this.getUserByEmail(user.email);
      loggedUser.token = token;
      return loggedUser;
    }
    throw new NotFoundException('Wrong password or e-mail', ErrorCodes.userNotFound);
  }

  async createUser(user: NewUserDto): Promise<User> {
    if (await this.isEmailAvailable(user.email)) {
      throw new DuplicateException(`User with email [${user.email}] exist!`, ErrorCodes.userWithEmailExist);
    }
    return this.saveUser(this.repository.create(user));
  }

  async saveUser(user: User): Promise<User> {
    return await this.repository.save(
        this.repository.create(user.isFirm ? user : {...user, userData: {}})
    );
  }

  async isEmailAvailable(email: string, id?: string): Promise<boolean> {
    let where;
    if (id) {
      where = {email, id: Not<string>(id)};
    } else {
      where = {email};
    }
    return (await this.repository.findOne({where})) !== undefined;
  }

  getUsers(id: string) {
    return Promise.resolve([]);
  }
}
