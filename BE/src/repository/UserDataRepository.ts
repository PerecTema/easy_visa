import {EntityRepository, getConnection, getRepository} from "typeorm";
import AbstractRepository from "~/repository/AbstractRepository";
import UserDataDto from "easy-visa-api/dto/UserDataDto";
import {UserData} from "~/entity/UserData";
import {BaseUserData} from "~/entity/BaseUserData";
import BaseEntity from "~/entity/BaseEntity";
import {BaseDataTypes} from "easy-visa-api/Enums/DataBaseEnums";
import BaseUserDataDto from "easy-visa-api/dto/BaseUserDataDto";
import { NotFoundException } from "easy-visa-api/exceptions/NotFoundException";
import { ErrorCodes } from "easy-visa-api/exceptions/ErrorCodes";
import {User} from "~/entity/User";


@EntityRepository(UserData)
export default class UserDataRepository extends AbstractRepository<UserData> {

    private repositoryBaseUserData;

    constructor() {
        super(UserData);
        this.repositoryBaseUserData = getRepository<BaseUserData>(BaseUserData);
    }

    async saveData(userDataDto: UserDataDto): Promise<UserData> {
        const userData = {} as UserData;
        getConnection().getMetadata(UserData).columns.forEach((column)=>{
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            userData[column.propertyName] = userDataDto[column.propertyName];
        });
        if (!userData.otherPeople) {
            userData.otherPeople = [];
        }
        userData.otherPeople.push(this.repositoryBaseUserData.create({
            ...userDataDto.mother,
            type: BaseDataTypes.mother
        }));
        userData.otherPeople.push(this.repositoryBaseUserData.create({
            ...userDataDto.father,
            type: BaseDataTypes.father
        }));
        userData.otherPeople.push(this.repositoryBaseUserData.create({
            ...userDataDto.spouse,
            type: BaseDataTypes.spouse
        }));
        (userDataDto.children || []).forEach(child => {
            userData.otherPeople.push(this.repositoryBaseUserData.create({
                ...child,
                type: BaseDataTypes.child
            }));
        });
        (userDataDto.brothersAndSisters || []).forEach(brotherOrSister => {
            userData.otherPeople.push(this.repositoryBaseUserData.create({
                ...brotherOrSister,
                type: BaseDataTypes.brotherOrSister
            }));
        });
        return this.repository.save(userData);
    }

    async getUserDataByUserId(id: typeof BaseEntity.prototype.id, user: User): Promise<UserDataDto> {
        const isSelfData = id === user.id;
        const userData = await this.repository.findOne({
            relations: ['user'],
            where: {
                user: {
                    id,
                    ...(isSelfData ? {} : {
                        parent: {
                            id: user.id,
                            isFirm: true
                        },
                    })
                }
            }
        });
        if (!userData) {
            throw new NotFoundException(`User not found`, ErrorCodes.userDataNotFound);
        }
        const userDataDto = userData as undefined as UserDataDto;
        (userData.otherPeople || []).forEach((human) => {
            switch (human.type) {
                case BaseDataTypes.child:
                    if (!userDataDto.children) userDataDto.children = [];
                    userDataDto.children.push(human as undefined as BaseUserDataDto);
                    break;
                case BaseDataTypes.brotherOrSister:
                    if (!userDataDto.brothersAndSisters) userDataDto.brothersAndSisters = [];
                    userDataDto.brothersAndSisters.push(human as undefined as BaseUserDataDto);
                    break;
                default:
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    userDataDto[human.type] = human as undefined as BaseUserDataDto;

            }
        });
        return userDataDto;
    }
}
