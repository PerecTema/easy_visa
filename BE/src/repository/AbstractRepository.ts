import {Repository, getRepository} from "typeorm";
import {EntityTarget} from "typeorm/common/EntityTarget";
import {DeepPartial} from "typeorm/common/DeepPartial";


export default abstract class AbstractRepository<Entity> {

  protected readonly repository: Repository<Entity>;

  protected constructor(entity: EntityTarget<Entity>) {
    this.repository = getRepository(entity);
  }

  createEntity(entity: DeepPartial<Entity>): Entity {
    return this.repository.create(entity);
  }


  // public async delete(entity: Entity, hard?: boolean): Promise<Entity> {
  //   if (hard) {
  //     return this.repository.remove(entity);
  //   } else {
  //     return this.repository.softRemove(entity);
  //   }
  // }
  //
  // public async create(entity: Entity): Promise<Entity> {
  //   return this.repository.save(entity);
  // }
  //
  // public async update(id: string, entity: Entity): Promise<Entity> {
  //   const exception = new NotFoundException(`Row with id [${id}] not updated`, ErrorCodes.rowNotFound);
  //   try {
  //     const row = await this.repository.findOneOrFail(id);
  //     if (!row) {
  //       throw exception;
  //     }
  //     const result = await this.repository.update(id, entity);
  //     if (result.affected === 0) {
  //       throw exception;
  //     }
  //     return Object.assign(row, entity);
  //   } catch (e) {
  //     throw exception;
  //   }
  // }

  public async findOne(entity: Entity): Promise<Entity | undefined> {
    return this.repository.findOne(entity);
  }

  public async findById(id: string): Promise<Entity | undefined> {
    return this.repository.findOne(id);
  }
}
