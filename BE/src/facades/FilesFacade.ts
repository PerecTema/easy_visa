import {NotFoundException} from "easy-visa-api/exceptions/NotFoundException";
import {ErrorCodes,} from "easy-visa-api/exceptions/ErrorCodes";
import fs from "fs";

const folder = `${__dirname}/../../files`;
export default class FilesFacade {

  static getFilePath(type: string) {
    const files = fs.readdirSync(folder).map((fileName: string) => fileName.split('.')[0]);
    if (files.indexOf(type) === -1) {
      throw new NotFoundException(`File [${type}] not found`, ErrorCodes.fileNotFound);
    }
    return `${folder}/${type}.pdf`;
  }
}
