import NewUserDto from "easy-visa-api/dto/NewUserDto";
import UserRepository from "~/repository/UserRepository";
import {User} from "~/entity/User";
import { ErrorCodes } from "easy-visa-api/exceptions/ErrorCodes";
import { NotFoundException } from "easy-visa-api/exceptions/NotFoundException";
import { PermissionDeniedException } from "easy-visa-api/exceptions/PermissionDeniedException";
import {TokenIsNotValidException} from "easy-visa-api/exceptions/TokenIsNotValidException";

export default class UserFacade {

  private readonly userRepository: UserRepository;


  constructor() {
    this.userRepository = new UserRepository();
  }

  async canUpdateOtherUser(user: User, otherUserId: string): Promise<{ user: User, otherUser: User }> {
    const otherUser = await this.userRepository.findById(otherUserId);
    if (!otherUser) {
      throw new NotFoundException(`User not found`, ErrorCodes.userDataNotFound);
    }
    if (otherUser.parent.id !== user.id) {
      throw new PermissionDeniedException(`You cant update this user`, ErrorCodes.userChangePermission);
    }
    return {user, otherUser};
  }

  async createUser(newUser: NewUserDto, needConfirm = true): Promise<User> {
    const user = await this.userRepository.createUser(newUser);
    if(!needConfirm){
      user.confirmEmailHash = null;
      await this.userRepository.saveUser(user);
    }
    return user;
  }

  getUserByEmail(email: string): Promise<User> {
    return this.userRepository.getUserByEmail(email);
  }

  verifyToken(token: string): Promise<User> {
    return this.userRepository.getUserByToken(token);
  }

  getLoginUser(token: string): Promise<User> {
    return this.userRepository.getUserByToken(token);
  }

  async loginUser(user: NewUserDto, confirmEmailHash?: string): Promise<User> {
    const _user = await this.userRepository.loginUser(user);

    if (await this.userRepository.isEmailConfirmed(_user.email)) {
      return _user;
    }
    if (_user.confirmEmailHash === confirmEmailHash) {
      return this.setConfirmEmail(_user);
    } else if (confirmEmailHash) {
      throw new TokenIsNotValidException('Wrong confirm e-mail hash');
    } else {
      throw new TokenIsNotValidException('You need confirm your e-mail');
    }
  }

  setConfirmEmail(user: User): Promise<User> {
    user.confirmEmailHash = null;
    delete user.password;
    return this.userRepository.saveUser(user);
  }

  async getUsers(user: User): Promise<User[]> {
    return user.clients;
  }

  async addClient(user: User, newUser: NewUserDto): Promise<User> {
    const createdUser = await this.createUser(newUser, false);
    createdUser.parent = user;
    return this.userRepository.saveUser(createdUser);
  }
}
