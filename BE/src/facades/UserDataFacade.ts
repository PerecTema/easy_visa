import UserDataDto from "easy-visa-api/dto/UserDataDto";
import UserDataRepository from "~/repository/UserDataRepository";
import UserRepository from "~/repository/UserRepository";
import {User} from "~/entity/User";
import type BaseEntity from "~/entity/BaseEntity";
import {UserData} from "~/entity/UserData";

export default class UserDataFacade {

  private readonly userDataRepository: UserDataRepository;
  private readonly userRepository: UserRepository;


  constructor() {
    this.userDataRepository = new UserDataRepository();
    this.userRepository = new UserRepository();
  }

  async updateUserData(user: User, userDataDto: UserDataDto): Promise<UserData> {
    user.userData = await this.userDataRepository.saveData(userDataDto);
    if(!userDataDto.id){
      await this.userRepository.saveUser(user);
    }
    return user.userData;
  }

  getUserDataByUserId(id: typeof BaseEntity.prototype.id, user: User): Promise<UserDataDto> {
    return this.userDataRepository.getUserDataByUserId(id, user) as undefined as Promise<UserDataDto>;
  }
}
