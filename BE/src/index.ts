import "reflect-metadata";
import 'module-alias/register';
import express from 'express';
import {env, loadEnv} from '~/types/env';
import {initDB} from "~/Connection";
import compression from "compression";
// const cors = require('cors');

loadEnv();

const router = () => require('~/controlers/index');

// const corsOptions = {
//   origin: `${env.FE_HOST}:${env.FE_PORT}`,
//   optionsSuccessStatus: 200
// };
initDB().then(() => {

  const app = express();
  const port = env.BE_PORT;

  // app.use(cors(corsOptions));

  app.use(compression());
  app.use(express.json());
  app.use(express.urlencoded({extended: true}));
  app.use(`/${env.API_VERSION}/`, router().default);
  app.listen(port, env.BE_HOST,() => {
    console.log(`Example app listening at ${env.BE_HOST}:${port}/${env.API_VERSION}/`);
  });
});





