import {Column, Entity, JoinColumn, ManyToOne,} from "typeorm";
import BaseEntity from "~/entity/BaseEntity";
import EmploymentDto from "easy-visa-api/dto/EmploymentDto";
import {Address} from "~/entity/Address";


@Entity({name: "Employments"})
export class Employment extends BaseEntity<EmploymentDto> {

  @Column({nullable: true,})
  public employerName!: string;

  @Column({nullable: true,})
  public position!: string;

  @ManyToOne(() => Address, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  public address!: Address;

}
