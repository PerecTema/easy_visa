import {Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToOne,} from "typeorm";
import BaseEntity from "~/entity/BaseEntity";
import type UserDataDto from "easy-visa-api/dto/UserDataDto";
import {CountryCodes, Sex, TypeOfTravelDocument, MaritalStatus, FormTypes} from "easy-visa-api/Enums/DataBaseEnums";
import {BaseUserData} from "~/entity/BaseUserData";
import {Address} from "~/entity/Address";
import {Employment} from "~/entity/Employment";
import {User} from "~/entity/User";


@Entity({name: "UsersData"})
export class UserData extends BaseEntity<UserDataDto> {

  @Column("enum", {enum: FormTypes})
  public formType!: FormTypes;

  @OneToOne(() => BaseUserData, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  baseData: BaseUserData;

  @ManyToMany(() => BaseUserData, (a) => a.userData, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
  }, ) @JoinTable()
  otherPeople: BaseUserData[];

  @Column("enum", {nullable: true, enum: MaritalStatus})
  public maritalStatus!: MaritalStatus;

  @Column("enum", {nullable: true, enum: Sex})
  public sex!: Sex;

  @Column("enum", {nullable: true, enum: TypeOfTravelDocument})
  public typeOfTravelDocument!: TypeOfTravelDocument;

  @Column({nullable: true,})
  public numberTravelDocument: string;

  @Column("enum", {nullable: true, enum: CountryCodes})
  public countryTravelDocument!: CountryCodes;

  @Column("enum", {nullable: true, enum: CountryCodes})
  public issuedBy!: CountryCodes;

  @Column({nullable: true, type: 'date'})
  public dateOfIssue!: Date;

  @Column({nullable: true, type: 'date'})
  public validUntilTravelDocument!: Date;

  @Column({nullable: true})
  public previousAndPlaceOfStay!: string;

  @Column({nullable: true})
  public purposeOfStayInCR!: string;

  @ManyToOne(() => Employment, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  public employmentBeforeCz!: Employment;

  @ManyToOne(() => Employment, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  public employmentInCzech!: Employment;

  @ManyToOne(() => Address, {
    cascade: true,
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  lastResidenceBeforeCz!: Address;

  @Column({nullable: true, type: 'date'})
  public previousStayFrom!: Date;

  @Column({nullable: true, type: 'date'})
  public previousStayTo!: Date;

  @Column({nullable: true, type: 'date'})
  public dateOfArrivalToCz!: Date;

  @OneToOne(() => User, (user) => user.userData)
  user: User;

  @Column({nullable: true,})
  public additionalInformation!: string;

}
