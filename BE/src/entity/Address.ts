import {Column, Entity,} from "typeorm";
import BaseEntity from "~/entity/BaseEntity";
import {CountryCodes} from "easy-visa-api/Enums/DataBaseEnums";
import AddressDto from "easy-visa-api/dto/AddressDto";


@Entity({name: "Addresses"})
export class Address extends BaseEntity<AddressDto> {

  @Column({nullable: true,})
  public street!: string;

  @Column({nullable: true,})
  public number!: string;

  @Column({nullable: true,})
  public town!: string;

  @Column({nullable: true,})
  public zip!: string;

  @Column({nullable: true,})
  public tel!: string;

  @Column('enum', {nullable: true, enum: CountryCodes})
  public state!: CountryCodes;

}
