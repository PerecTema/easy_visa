import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity, Generated,
  JoinColumn, ManyToOne, OneToMany,
  OneToOne,
} from "typeorm";
import BaseEntity from "~/entity/BaseEntity";
import UserDto from "easy-visa-api/dto/UserDto";
import {hash} from "~/repository/UserRepository";
import {UserData} from "~/entity/UserData";


@Entity({name: "Users"})
export class User extends BaseEntity<UserDto> {

  private tempPassword: string;

  @Column({unique: true,})
  email: string;

  @Column({select: false})
  public password: string;

  @Column({nullable: true, type: 'uuid', select: false})
  public token!: string | null;


  @Generated("uuid")
  @Column({nullable: false, type: 'uuid'})
  tenant: string;

  @Column({default: false, type: 'boolean'})
  isFirm = false;

  @Generated("uuid")
  @Column({nullable: true, type: 'uuid', select: false})
  public confirmEmailHash: string;

  @AfterLoad()
  private loadTempPassword(): void {
    this.tempPassword = this.password;
  }

  @BeforeInsert()
  @BeforeUpdate()
  private encryptPassword(): void {
    if (this.id) {
      return;
    }
    if (this.tempPassword !== this.password) {
      this.password = hash(this.tempPassword ?? this.password);
    }
  }

  @OneToOne(() => UserData, {
    cascade: true,
    nullable: true,
    lazy: true,
  })
  @JoinColumn()
  userData!: UserData;

  @OneToMany(() => User, user=>user.parent, {
    lazy: true,
  })
  clients: User[];

  @ManyToOne(() => User, user => user.clients, {
    lazy: true,
  })
  parent: User;

  toJSON(): UserDto {
    const data = super.toJSON();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete data.password;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete data.tempPassword;
    return data;
  }
}
