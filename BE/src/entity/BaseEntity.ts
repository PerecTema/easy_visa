import {CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

export default class BaseEntity<DTO> {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @CreateDateColumn()
  created!: Date;

  @UpdateDateColumn()
  updated!: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  toJSON(): DTO {
    return {...this} as unknown as DTO;
  }
}
