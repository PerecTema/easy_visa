import {Column, Entity, JoinColumn, ManyToMany, OneToOne,} from "typeorm";
import BaseEntity from "~/entity/BaseEntity";
import {CountryCodes, Education, BaseDataTypes} from "easy-visa-api/Enums/DataBaseEnums";
import {Address} from "~/entity/Address";
import {UserData} from "~/entity/UserData";


@Entity({name: "BaseUsersData"})
export class BaseUserData<Entity = typeof BaseUserData> extends BaseEntity<Entity> {

    @Column("enum", {nullable: false, enum: BaseDataTypes})
    public type!: string;

    @Column({nullable: true,})
    public surname!: string;

    @Column({nullable: true,})
    public surnameOther!: string;

    @Column({nullable: true,})
    public surnameAtBirth!: string;

    @Column({nullable: true,})
    public firstName!: string;

    @Column({nullable: true,})
    public firstNameAtBirth!: string;

    @Column({nullable: true,})
    public otherNames!: string;

    @Column({type: 'date', nullable: true,})
    public dateOfBirth!: string;

    @Column({nullable: true,})
    public placeOfBirth!: string;

    @Column({nullable: true,})
    public occupation!: string;

    @Column("enum", {nullable: true, enum: Education})
    public education!: Education;

    @Column("enum", {nullable: true, enum: CountryCodes})
    public countryOfBird!: CountryCodes;

    @Column("enum", {nullable: true, enum: CountryCodes})
    public originalNationally!: CountryCodes;

    @Column("enum", {nullable: true, enum: CountryCodes})
    public currentNationally!: CountryCodes;

    @OneToOne(() => Address, {
        cascade: true,
        onDelete: 'CASCADE',
        eager: true,
    })
    @JoinColumn()
    public addressInCZ!: Address;

    @OneToOne(() => Address, {
        cascade: true,
        onDelete: 'CASCADE',
        eager: true,
    })
    @JoinColumn()
    public residence!: Address;

    @OneToOne(() => Address, {
        cascade: true,
        onDelete: 'CASCADE',
        eager: true,
    })
    @JoinColumn()
    public addressInCZDelivery!: Address;

    @OneToOne(() => UserData, (a) => a.baseData)
    mainUser: UserData;

    @ManyToMany(() => UserData, (a) => a.otherPeople)
    userData: UserData[];

}
