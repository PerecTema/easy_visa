// import {createConnection, getConnection} from 'typeorm';
// import {Connection} from "typeorm/connection/Connection";
import {Connection, getConnectionManager, getConnectionOptions} from "typeorm";
let connection;

// const connection = {
//   async create(connectionName: string): Promise<Connection> {
//     return connectionName === undefined ? createConnection() : createConnection(connectionName);
//   },
//
//   async close(connectionName: string): Promise<void> {
//     await getConnection(connectionName).close();
//   },
//
//   async clear(connectionName: string): Promise<void> {
//     const connection = getConnection(connectionName);
//     const entities = connection.entityMetadatas;
//
//     for (const entity of entities) {
//       const repository = connection.getRepository(entity.name);
//       await repository.query(`DELETE FROM ${entity.tableName}`);
//     }
//   },
// };
export async function initDB(): Promise<void> {
 connection = await getConnectionManager().create(await getConnectionOptions()).connect();
}
export default connection as Connection;
