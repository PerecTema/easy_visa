import { EnvType, load } from 'ts-dotenv';
import * as dotenv from "dotenv";

export const schema = {
  NODE_ENV: [
    'production' as const,
    'development' as const,
  ],
  FE_PORT: Number,
  FE_HOST: String,
  BE_PORT: Number,
  BE_HOST: String,
  DB_HOST: String,
  DB_NAME: String,
  DB_PASS: String,
  DB_USER: String,
  API_VERSION: String,
};

export type Env = EnvType<typeof schema>;

export function loadEnv(): void {
  dotenv.config();
  env = load(schema);
}

export let env: Env;
