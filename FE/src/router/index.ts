import { createRouter, createWebHashHistory } from "vue-router";
import { useUserStore } from "@/stores/UserStore";
import landingPage from "@/modules/landingPage/router";

export enum Routes {
  home = "home",
  login = "login",
  register = "register",
  forms = "forms",
  users = "users",
  profile = "profile",
}

export const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  scrollBehavior(to) {
    if (to.hash) {
      return {
        el: to.hash,
        behavior: "smooth",
      };
    }
  },
  routes: [
    {
      path: "/home",
      component: () => import("@/layouts/DefaultLayout.vue"),
      beforeEnter: async (to, from, next) => {
        const userStore = useUserStore();
        await userStore.fetchUser();
        if (!userStore.userData) {
          await userStore.logout();
        }
        next();
      },
      children: [
        {
          path: "/:id?/forms/:type?",
          name: Routes.forms,
          component: () => import("@/views/HomeView.vue"),
        },
        {
          path: "/users",
          name: Routes.users,
          component: () => import("@/views/UsersListPage.vue"),
          beforeEnter: async (to, from, next) => {
            const userStore = useUserStore();
            if (!userStore.userData.isFirm) {
              next({ name: Routes.home });
            }
            next();
          },
        },
      ],
    },
    {
      path: "/home/signup",
      component: () => import("@/layouts/LoginLayout.vue"),
      children: [
        {
          path: "login/:confirmEmailHash?",
          name: Routes.login,
          component: () => import("@/views/LoginPage.vue"),
        },
        {
          path: "registration",
          name: Routes.register,
          component: () => import("@/views/RegisterPage.vue"),
        },
      ],
    },
    ...landingPage,
  ],
});

export default router;
