import { defineStore } from "pinia";
import type NewUserDto from "easy-visa-api/dto/NewUserDto";
import type UserDto from "easy-visa-api/dto/UserDto";
import { DuplicateException } from "easy-visa-api/exceptions/DuplicateException";
import { NotFoundException } from "easy-visa-api/exceptions/NotFoundException";
import { TokenIsNotValidException } from "easy-visa-api/exceptions/TokenIsNotValidException";
import { ErrorCodes } from "easy-visa-api/exceptions/ErrorCodes";
import {
  createUser,
  getUser,
  loginUser,
  putUserData,
  getUserData,
  getMyUsers,
  addClient,
} from "@/api/UserApi";
import { useCookies } from "vue3-cookies";
import { Cookies } from "@/Enums";
import type UserDataDto from "easy-visa-api/dto/UserDataDto";
const { cookies } = useCookies();

export const useUserStore = defineStore({
  id: "userStore",
  state: () => ({
    userData: {} as UserDto,
  }),
  getters: {},
  actions: {
    async registerUser(userData: NewUserDto): Promise<UserDto | null> {
      try {
        return await createUser(userData);
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userWithEmailExist) {
          throw new DuplicateException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
        return null;
      }
    },
    async addClient(userData: NewUserDto): Promise<UserDto | null> {
      try {
        return await addClient(userData);
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userWithEmailExist) {
          throw new DuplicateException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
        return null;
      }
    },
    async fetchUser(): Promise<UserDto | null> {
      try {
        this.userData = await getUser();
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userNotFound) {
          throw new NotFoundException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
      }
      return null;
    },
    async getMyUsers(): Promise<UserDto[]> {
      try {
        return await getMyUsers();
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userNotFound) {
          throw new NotFoundException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
      }
      return [];
    },
    async putUserData(data: UserDataDto): Promise<UserDataDto | null> {
      try {
        return putUserData(data);
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userDataNotFound) {
          throw new NotFoundException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
      }
      return null;
    },
    async getUserData(id?: string): Promise<UserDataDto | null> {
      try {
        return getUserData(id);
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userDataNotFound) {
          throw new NotFoundException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
      }
      return null;
    },
    async loginUser(
      userData: NewUserDto,
      confirmEmailHash?: string
    ): Promise<UserDto | null> {
      try {
        this.userData = await loginUser(userData, confirmEmailHash);
        cookies.set(Cookies.token, this.userData.token);
        return this.userData;
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userNotFound) {
          throw new NotFoundException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
        if (e?.response?.data.code === ErrorCodes.tokenIsNotValid) {
          throw new TokenIsNotValidException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
      }
      return null;
    },
    async logout(): Promise<void> {
      cookies.remove(Cookies.token);
    },
  },
});
