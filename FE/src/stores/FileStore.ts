import { defineStore } from "pinia";
import { DuplicateException } from "easy-visa-api/exceptions/DuplicateException";
import { ErrorCodes } from "easy-visa-api/exceptions/ErrorCodes";
import { getPdfTemplate } from "@/api/FilesApi";
import type { Forms } from "@/Enums";

export const useFileStore = defineStore({
  id: "fileStore",
  state: () => ({}),
  getters: {},
  actions: {
    async getPdfTemplate(type: Forms): Promise<Blob | null> {
      try {
        return await getPdfTemplate(type);
      } catch (e) {
        if (e?.response?.data.code === ErrorCodes.userWithEmailExist) {
          throw new DuplicateException(
            e?.response?.data.message,
            e?.response?.data.code
          );
        }
      }
      return null;
    },
  },
});
