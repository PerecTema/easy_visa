import VueToast, { type ToastProps } from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
export default VueToast;

import { type ToastPluginApi, useToast } from "vue-toast-notification";

const toast = useToast() as ToastPluginApi;

export const successToast = (message: string, options?: ToastProps) => {
  toast.success(message, options);
};
export const errorToast = (message: string, options?: ToastProps) => {
  toast.error(message, options);
};
export const infoToast = (message: string, options?: ToastProps) => {
  toast.info(message, options);
};
export const warningToast = (message: string, options?: ToastProps) => {
  toast.warning(message, options);
};
