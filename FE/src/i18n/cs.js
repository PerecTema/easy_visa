export default {
  enums: {
    education: {
      basic: "základní",
      highSchool: "střední bez maturity",
      apprenticeshipCertificate: "střední s výučním listem",
      schoolLeaving: "střední s maturitní zkouškou",
      grammarSchool: "gymnázium",
      professional: "vyšší odborné vzdělání",
      bachelor: "vš bakalář",
      master: "vš Mgr., Ing.",
      doctoral: "vš Ph.D.",
    },
  },
};
