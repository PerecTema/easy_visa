// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import en from "@/i18n/en";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import cs from "@/i18n/cs";
import landingPage from "@/modules/landingPage/i18n";
import { createI18n } from "vue-i18n";
import { merge } from "lodash";

export enum Locales {
  "en" = "en",
  "cs" = "cs",
}

export const i18n = createI18n({
  locale: Locales.en,
  messages: merge(
    {
      en,
      cs,
    },
    landingPage
  ),
});

export const i18nGlobal = i18n.global;
