import { MaritalStatus } from "easy-visa-api/Enums/DataBaseEnums";
import { Routes } from "@/router";
import en from "@vee-validate/i18n/dist/locale/en.json";
import { get } from "lodash";

const translate = {
  menu: {
    title: "easy visa",
    logout: "logout",
  },
  router: {
    [Routes.profile]: "profile",
    [Routes.forms]: "forms",
    [Routes.users]: "users list",
  },
  errors: {
    mixed: {
      confirmPassword: "Passwords must match",
      required: "{label} is required",
    },
    string: {
      min: "{label} must be at least {number} characters",
      email: "{label} must be a valid email",
      sameAs: "{label1} must be same as {label2}",
    },
  },
  enums: {
    education: {
      basic: "basic education or basics of education",
      highSchool: "high school education",
      apprenticeshipCertificate:
        "secondary education with an apprenticeship certificate",
      schoolLeaving: "secondary education with a school-leaving examination",
      professional: "higher professional education",
      grammarSchool: "grammar school",
      bachelor: "higher education in a bachelor's degree program",
      master: "higher education in a master's degree program",
      doctoral: "higher education in a doctoral study program",
    },
    formTypes: {
      application: "application",
      permanent: "for permanent residence permit",
      long: "for long term residence permit",
      extension:
        "for the extension of validity period of long term residence permit",
      extensionChange:
        "for the extension of validity of the resident alien permit - for the issue of a substitute resident alien permit card - exchange of a residence permit decision for a resident alien card",
    },
    sexTypes: {
      male: "male", // muž
      female: "female", // žena
    },
    maritalStatus: {
      [MaritalStatus.single]: "single", // svobodný/á
      [MaritalStatus.widow]: "widowed", // vdovec/vdova
      [MaritalStatus.married]: "married", // ženatý/vdaná
      [MaritalStatus.divorced]: "divorced", // rozvedený/á
    },
    countryCodes: {
      AFG: "Afghanistan",
      ALA: "Åland Island",
      ALB: "Albania",
      DZA: "Algeria",
      ASM: "American Samoa",
      VIR: "Virgin Islands (U. S.)",
      AND: "Andorra",
      AGO: "Angola",
      AIA: "Anguilla",
      ATA: "Antarctica",
      ATG: "Antigua and Barbuda",
      ARG: "Argentina",
      ARM: "Armenia",
      ABW: "Aruba",
      AUS: "Australia",
      AZE: "Azerbaijan",
      BHS: "Bahamas (the)",
      BHR: "Bahrain",
      BGD: "Bangladesh",
      BRB: "Barbados",
      BEL: "Belgium",
      BLZ: "Belize",
      BLR: "Belarus",
      BEN: "Benin",
      BMU: "Bermuda",
      BTN: "Bhutan",
      BOL: "Bolivia (Plurinational State of)",
      BES: "Bonaire, Sint Eustatius and Saba",
      BIH: "Bosnia and Herzegovina",
      BWA: "Botswana",
      BVT: "Bouvet Island",
      BRA: "Brazil",
      IOT: "British Indian Ocean Territory (the)",
      VGB: "Virgin Islands (British)",
      BRN: "Brunei Darussalam",
      BGR: "Bulgaria",
      BFA: "Burkina Faso",
      BDI: "Burundi",
      COK: "Cook Islands (the)",
      CUW: "Curaçao",
      TCD: "Chad",
      MNE: "Montenegro",
      CZE: "Czechia",
      CHN: "China",
      DNK: "Denmark",
      COD: "Congo (the Democratic Republic of the)",
      DMA: "Dominica",
      DOM: "Dominican Republic (the)",
      DJI: "Djibouti",
      EGY: "Egypt",
      ECU: "Ecuador",
      ERI: "Eritrea",
      EST: "Estonia",
      ETH: "Ethiopia",
      FRO: "Faroe Islands (the)",
      FLK: "Falkland Islands (the) (Malvinas)",
      FJI: "Fiji",
      PHL: "Philippines (the)",
      FIN: "Finland",
      FRA: "France",
      GUF: "French Guiana",
      ATF: "French Southern Territories (the)",
      PYF: "French Polynesia",
      GAB: "Gabon",
      GMB: "Gambia (the)",
      GHA: "Ghana",
      GIB: "Gibraltar",
      GRD: "Grenada",
      GRL: "Greenland",
      GEO: "Georgia",
      GLP: "Guadeloupe",
      GUM: "Guam",
      GTM: "Guatemala",
      GGY: "Guernsey",
      GIN: "Guinea",
      GNB: "Guinea-Bissau",
      GUY: "Guyana",
      HTI: "Haiti",
      HMD: "Heard Island and McDonald Islands",
      HND: "Honduras",
      HKG: "Hong Kong",
      CHL: "Chile",
      HRV: "Croatia",
      IND: "India",
      IDN: "Indonesia",
      IRQ: "Iraq",
      IRN: "Iran (Islamic Republic of)",
      IRL: "Ireland",
      ISL: "Iceland",
      ITA: "Italy",
      ISR: "Israel",
      JAM: "Jamaica",
      JPN: "Japan",
      YEM: "Yemen",
      JEY: "Jersey",
      ZAF: "South Africa",
      SGS: "South Georgia and the South Sandwich Islands",
      SSD: "South Sudan",
      JOR: "Jordan",
      CYM: "Cayman Islands (the)",
      KHM: "Cambodia",
      CMR: "Cameroon",
      CAN: "Canada",
      CPV: "Cape Verde",
      QAT: "Qatar",
      KAZ: "Kazakhstan",
      KEN: "Kenya",
      KIR: "Kiribati",
      CCK: "Cocos (Keeling) Islands (the)",
      COL: "Colombia",
      COM: "Comoros (the)",
      COG: "Congo (the)",
      PRK: "Korea (the Democratic People's Republic of)",
      KOR: "Korea (the Republic of)",
      XXK: "Kosovo",
      CRI: "Costa Rica",
      CUB: "Cuba",
      KWT: "Kuwait",
      CYP: "Cyprus",
      KGZ: "Kyrgyzstan",
      LAO: "Lao People's Democratic Republic (the)",
      LSO: "Lesotho",
      LBN: "Lebanon",
      LBR: "Liberia",
      LBY: "Libya",
      LIE: "Liechtenstein",
      LTU: "Lithuania",
      LVA: "Latvia",
      LUX: "Luxembourg",
      MAC: "Macao",
      MDG: "Madagascar",
      HUN: "Hungary",
      MKD: "North Macedonia",
      MYS: "Malaysia",
      MWI: "Malawi",
      MDV: "Maldives",
      MLI: "Mali",
      MLT: "Malta",
      IMN: "Isle of Man",
      MAR: "Morocco",
      MHL: "Marshall Islands (the)",
      MTQ: "Martinique",
      MUS: "Mauritius",
      MRT: "Mauritania",
      MYT: "Mayotte",
      UMI: "United States Minor Outlying Islands (the)",
      MEX: "Mexico",
      FSM: "Micronesia (Federated States of)",
      MDA: "Moldova (the Republic of)",
      MCO: "Monaco",
      MNG: "Mongolia",
      MSR: "Montserrat",
      MOZ: "Mozambique",
      MMR: "Myanmar",
      NAM: "Namibia",
      NRU: "Nauru",
      DEU: "Germany",
      NPL: "Nepal",
      NER: "Niger (the)",
      NGA: "Nigeria",
      NIC: "Nicaragua",
      NIU: "Niue",
      NLD: "Netherlands (the)",
      NFK: "Norfolk Island",
      NOR: "Norway",
      NCL: "New Caledonia",
      NZL: "New Zealand",
      OMN: "Oman",
      PAK: "Pakistan",
      PLW: "Palau",
      PSE: "Palestine, State of",
      PAN: "Panama",
      PNG: "Papua New Guinea",
      PRY: "Paraguay",
      PER: "Peru",
      PCN: "Pitcairn",
      CIV: "Côte d'Ivoire",
      POL: "Poland",
      PRI: "Puerto Rico",
      PRT: "Portugal",
      AUT: "Austria",
      REU: "Réunion",
      GNQ: "Equatorial Guinea",
      ROU: "Romania",
      RUS: "Russian Federation (the)",
      RWA: "Rwanda",
      GRC: "Greece",
      SPM: "Saint Pierre and Miquelon",
      SLV: "El Salvador",
      WSM: "Samoa",
      SMR: "San Marino",
      SAU: "Saudi Arabia",
      SEN: "Senegal",
      MNP: "Northern Mariana Islands (the)",
      SYC: "Seychelles",
      SLE: "Sierra Leone",
      SGP: "Singapore",
      SVK: "Slovakia",
      SVN: "Slovenia",
      SOM: "Somalia",
      ARE: "United Arab Emirates (the)",
      USA: "United States of America (the)",
      SRB: "Serbia",
      CAF: "Central African Republic (the)",
      SDN: "Sudan (the)",
      SUR: "Suriname",
      SHN: "Saint Helena, Ascension and Tristan da Cunha",
      LCA: "Saint Lucia",
      BLM: "Saint Barthélemy",
      KNA: "Saint Kitts and Nevis",
      MAF: "Saint Martin (French part)",
      SXM: "Sint Maarten (Dutch part)",
      STP: "Sao Tome and Principe",
      VCT: "Saint Vincent and the Grenadines",
      SWZ: "Eswatini",
      SYR: "Syrian Arab Republic",
      SLB: "Solomon Islands",
      ESP: "Spain",
      SJM: "Svalbard and Jan Mayen",
      LKA: "Sri Lanka",
      SWE: "Sweden",
      CHE: "Switzerland",
      TJK: "Tajikistan",
      TZA: "Tanzania, United Republic of",
      THA: "Thailand",
      TWN: "Taiwan (Province of China)",
      TGO: "Togo",
      TKL: "Tokelau",
      TON: "Tonga",
      TTO: "Trinidad and Tobago",
      TUN: "Tunisia",
      TUR: "Turkey",
      TKM: "Turkmenistan",
      TCA: "Turks and Caicos Islands (the)",
      TUV: "Tuvalu",
      UGA: "Uganda",
      UKR: "Ukraine",
      URY: "Uruguay",
      UZB: "Uzbekistan",
      CXR: "Christmas Island",
      VUT: "Vanuatu",
      VAT: "Holy See (the)",
      GBR: "United Kingdom of Great Britain and Northern Ireland (the)",
      VEN: "Venezuela (Bolivarian Republic of)",
      VNM: "Viet Nam",
      TLS: "Timor-Leste",
      WLF: "Wallis and Futuna",
      ZMB: "Zambia",
      ESH: "Western Sahara",
      ZWE: "Zimbabwe",
    },
  },
  form: {
    message: {
      success: {
        registration: "Registration was successful",
        userAdded: "Added new user",
        login: "Login",
        logout: "Logout",
      },
    },
    field: {
      login: "login",
      registration: "sign up",
      submit: "submit",
      add: "add",
      close: "close",
      save: "save",
      addUser: "add user",
      remove: "remove",
      addChildren: "add children",
      fillForm: "fill form",
      //btn
      addBrothersAndSisters: "add siblings",
      email: "e-mail",
      registerAsFirm: "sign up as firm",
      password: "password",
      repeatPassword: "confirm password",
      surname: "surname", //příjmení
      otherNames: "other names", //ostatní jména
      formType: "application type",
      surnameAtBirth: "all previous surnames", //všechna dřívější příjmení
      firstName: "first name(s)", //jméno(a)
      firstNameAtBirth: "",
      sex: "sex", //pohlaví
      maritalStatus: "marital status", //rodinný stav
      dateOfBirth: "date of birth", //datum narození
      placeOfBirth: "place of birth", //místo narození
      occupation: "occupation", //povolání
      countryOfBird: "country of birth", //stát narození
      originalNationally: "",
      currentNationally: "nationality", //státní občanství
      addressInCZ: "",
      addressInCZDelivery: "",
      education: "education", // nejvyšší dosažené vzdělání
      employmentBeforeCz: "employment before arrival to the czech republic", // zaměstnání před příchodem na území
      employmentInCzech: "employment after arrival to the czech republic", // zaměstnání po vstupu na území
      //employer
      employerName: "employer", // zaměstnavatel
      position: "position", // pracovní zařazení
      //employer
      //address
      street: "street", //ulice
      number: "number", // číslo
      town: "town", // město
      zip: "post code", // psč
      tel: "", //
      state: "country", //stát
      //address
      purposeOfStayInCR: "purpose of stay in the czech republic", // účel pobytu na území
      lastResidenceBeforeCz: "last residence abroad", // poslední bydliště v cizině
      previousStay: "previous stay in the czech republic longer than 3 months", // předchozí pobyt na území delší než 3 měsíce
      previousStayFrom: "stay from", // pobyt od
      previousStayTo: "till", // do
      previousAndPlaceOfStay: "purpose and place of stay", //důvod a místo pobytu
      residence: "residence address", // adresa místa pobytu
      residenceInCz: "residence address in the czech republic", // adresa místa pobytu na území
      residenceCorespondentInCz:
        "delivery address if different from residence address", // adresa pro doručování, je-li odlišná od místa pobytu
      dateOfArrivalToCz: "date of arrival to the czech republic", // den vstupu na území
      countryTravelDocument: "travel document issuing country", // země, která vydala cestovní doklad
      numberTravelDocument: "travel document number", // číslo cestovního dokladu
      validUntilTravelDocument: "travel document valid until", // platnost cestovního dokladu
      spouse: "spouse", // manžel(ka)
      children: "children", // děti
      father: "father", // otec
      mother: "mother", // matka
      brothersAndSisters: "brothers and sisters", // sourozenci
      additionalInformation: "additional information", // doplňující informace
      brotherOrSister: "brother or sister",
    },
  },
  pages: {
    forms: {
      listForms: "forms",
      users: "users list",
    },
  },
};
export default translate;
const getTranslate = (field) => {
  return get(translate.form.field, field.split(".").reverse().shift(), field);
};
export const veeI18n = {
  messages: {
    ...en.messages,
    ...{
      sameAs: (context) =>
        `The ${getTranslate(context.field)} 
          field may by sam as ${getTranslate(context.rule.params[0])}`,
      required: (context) => `The ${getTranslate(context.field)} is required.`,
    },
  },
};
