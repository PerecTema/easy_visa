import type { Directive } from "vue";

function listenerClickOutside(
  event: MouseEvent,
  ignoreClickOnMeElement: HTMLElement,
  params: { value: () => void }
) {
  if (!(event.target instanceof HTMLElement)) {
    return;
  }
  const isClickInsideElement = ignoreClickOnMeElement.contains(event.target);
  if (!isClickInsideElement) {
    params.value();
  }
}
export default {
  "click-outside": {
    mounted: function (
      ignoreClickOnMeElement: HTMLElement,
      params: { value: () => void }
    ) {
      document.addEventListener("click", (event) =>
        listenerClickOutside(event, ignoreClickOnMeElement, params)
      );
    },
    unmounted: function (
      ignoreClickOnMeElement: HTMLElement,
      params: { value: () => void }
    ) {
      document.removeEventListener("click", (event) =>
        listenerClickOutside(event, ignoreClickOnMeElement, params)
      );
    },
  },
} as { [key: string]: Directive };
