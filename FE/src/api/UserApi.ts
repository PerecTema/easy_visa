import type NewUserDto from "easy-visa-api/dto/NewUserDto";
import type UserDataDto from "easy-visa-api/dto/UserDataDto";
import type UserDto from "easy-visa-api/dto/UserDto";
import axios from "@/api/axios";
import { RoutUserEnums } from "easy-visa-api/Enums/RoutEnums";

export const createUser = async (data: NewUserDto): Promise<UserDto> => {
  return (
    await axios.post(`/${RoutUserEnums.user}/${RoutUserEnums.userCreate}`, data)
  ).data;
};

export const addClient = async (data: NewUserDto): Promise<UserDto> => {
  return (
    await axios.post(`/${RoutUserEnums.user}/${RoutUserEnums.addUser}`, data)
  ).data;
};

export const putUserData = async (data: UserDataDto): Promise<UserDataDto> => {
  return (
    await axios.put(`/${RoutUserEnums.user}/${RoutUserEnums.putUserData}`, data)
  ).data;
};

export const getUserData = async (id?: string): Promise<UserDataDto> => {
  return (
    await axios.get(`/${RoutUserEnums.user}/${RoutUserEnums.getUserData}`, {
      params: id ? { id } : {},
    })
  ).data;
};

export const loginUser = async (
  data: NewUserDto,
  confirmEmailHash?: string
): Promise<UserDto> => {
  return (
    await axios.post(
      `/${RoutUserEnums.user}/${RoutUserEnums.userLogin}/${confirmEmailHash}`,
      data
    )
  ).data as UserDto;
};

export const getUser = async (): Promise<UserDto> => {
  return (await axios.get(`/${RoutUserEnums.user}/${RoutUserEnums.userGet}`))
    .data as UserDto;
};

export const getMyUsers = async (): Promise<UserDto[]> => {
  return (
    await axios.get(`/${RoutUserEnums.user}/${RoutUserEnums.getUsersList}`)
  ).data;
};
