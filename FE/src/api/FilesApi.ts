import axios from "@/api/axios";
import { RoutFilesEnums } from "easy-visa-api/Enums/RoutEnums";
import type { Forms } from "@/Enums";

export const getPdfTemplate = async (type: Forms): Promise<Blob> => {
  return (
    await axios.get(`/${RoutFilesEnums.files}/`, {
      params: { type },
      responseType: "blob",
    })
  ).data;
};
