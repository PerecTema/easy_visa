import instance from "axios";
import { Cookies } from "@/Enums";
import { useCookies } from "vue3-cookies";
import { errorToast } from "@/plugins/Toast";
import { useUserStore } from "@/stores/UserStore";
import router from "@/router";
const { cookies } = useCookies();

const axios = instance.create({
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  baseURL: `/${process.env.API_VERSION}/`,
  timeout: 1000,
});

axios.interceptors.request.use((request) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  request.headers.common.authorization = cookies.get(Cookies.token);
  return request;
});

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    if (401 === error.response.status) {
      await useUserStore().logout();
      errorToast("logout");
      await router.push({ name: "login" });
    }
    return Promise.reject(error);
  }
);

export default axios;
