import { configure, defineRule } from "vee-validate";
import { get } from "lodash";
import { localize } from "@vee-validate/i18n";
import rules from "@/vee/Rules";
import { veeI18n } from "@/i18n/en";

configure({
  validateOnBlur: true,
  validateOnModelUpdate: true,
  bails: true,
  validateOnChange: true,
  validateOnInput: true,
});

Object.keys(rules).forEach((rule) => {
  if (typeof get(rules, rule) !== "function") {
    return;
  }
  defineRule(rule, get(rules, rule));
});

configure({
  generateMessage: localize({
    en: veeI18n,
  }),
});
