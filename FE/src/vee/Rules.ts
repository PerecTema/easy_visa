import * as rules from "@vee-validate/rules";
import { useField } from "vee-validate";
import { get } from "lodash";
import type { Ref } from "vue";

/////sameAs//////////////
const sameAsValues: { [key: string]: Ref } = {};
const sameAs = (val: string, params: Array<string>) => {
  const fieldNameForCompare = params[0];
  if (!get(sameAsValues, fieldNameForCompare, false)) {
    const { value } = useField(params[0]);
    sameAsValues[fieldNameForCompare] = value;
  }
  return sameAsValues[fieldNameForCompare].value === val;
};
/////sameAs//////////////

export default { ...rules, sameAs };
