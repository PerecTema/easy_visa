import { createApp } from "vue";
import { createPinia } from "pinia";
import "./assets/index.css";
import "./vee/Config";
import { i18n } from "@/i18n";
import VueToast from "@/plugins/Toast";
import directives from "@/directives/index";

import App from "./App.vue";
import router from "./router";

export const VueApp = createApp(App);

Object.keys(directives).forEach((key) =>
  VueApp.directive(key, directives[key])
);

VueApp.use(createPinia());
VueApp.use(router);
VueApp.use(VueToast);
VueApp.use(i18n);
VueApp.mount("#app");
