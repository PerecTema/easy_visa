import type { RouteRecordRaw } from "vue-router";

export enum Routes {
  home = "home",
}
const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: () => import("@/modules/landingPage/views/Layout.vue"),
    // beforeEnter: async (to, from, next) => {
    //   next();
    // },
  },
];
export default routes;
