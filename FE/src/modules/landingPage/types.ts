import type { RouteLocationRaw } from "vue-router";

export type MenuItemType = { link: RouteLocationRaw; label: string };
