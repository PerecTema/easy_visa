export default {
  banner: {
    title:
      "Getting a visa to the Czech Republic is difficult? Not! Visa&nbsp;Is&nbsp;Simple.",
    text: "You only need to click a few buttons to get a visa.",
    doIt: "to get the visa",
  },
  footer: {
    autorise: "autorise",
    contacts: "contacts",
  },
};
