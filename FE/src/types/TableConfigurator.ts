import { ref } from "vue";

export type rowType<DTO> = { [K in keyof DTO]?: fieldType };

export type fieldType = {
  label: string;
  sort?: "ASC" | "DESC";
  displayOrder?: number;
  displayComponent?: {
    component: any;
    attr: object;
  };
  _key?: string; // not write here. Well be overwrite
};
export type actionType<R> = {
  label?: string;
  displayOrder?: number;
  position?: "append" | "prepend";
  displayComponent: {
    component: any;
    click: (row: R) => void;
    attr: object;
  };
  _key?: string;
};
export default class TableConfigurator<DTO extends { id: string }> {
  items = ref<DTO[] | undefined>(undefined);
  fields = ref<rowType<DTO>>({});
  actions = ref<actionType<rowType<DTO>>[]>([]);
  primaryKey: keyof DTO = "id";
}
