import { PDFDocument, PDFFont, PDFPage } from "pdf-lib";
import type UserDataDto from "easy-visa-api/dto/UserDataDto";
import type BaseUserDataDto from "easy-visa-api/dto/BaseUserDataDto";
import {
  Sex,
  MaritalStatus,
  FormTypes,
} from "easy-visa-api/Enums/DataBaseEnums";
import { getDateFrom } from "@/helpers/DateHelper";
import { i18nGlobal, Locales } from "@/i18n";
import fontkit from "@pdf-lib/fontkit";
import { get } from "lodash";

export class PDFApplicationService {
  private _file: Blob;
  private _font!: PDFFont;
  private config = {
    offsetXStart: 82,
    offsetYStart: 579,
    stepX: 14.1,
    stepY: 16,
    fontSize: 14,
    maxOffsetX: 31,
    calibrationYValue: 0,
  };

  constructor(file: Blob) {
    this._file = file;
  }

  private setHusband(
    position: number,
    page: PDFPage,
    data: BaseUserDataDto
  ): void {
    this.writeText(get(data, "surname"), page, position, 0);
    this.writeText(get(data, "otherNames"), page, position + 2, 0, true);
    this.writeText(get(data, "firstName"), page, position + 4, 11);
    this.writeText(
      getDateFrom(get(data, "dateOfBirth")),
      page,
      position + 5,
      25
    );
    this.writeText(get(data, "currentNationally"), page, position + 6, 30);
    this.writeText(get(data, "occupation"), page, position + 7, 10);
    if (data.residence) {
      this.writeText(
        `${data.residence.state || ""} ${data.residence.town || ""}` +
          ` ${data.residence.street || ""} ${data.residence.number || ""}`,
        page,
        position + 9,
        0
      );
    }
  }

  private setSistersAndBrothers(
    position: number,
    page: PDFPage,
    data: BaseUserDataDto
  ): void {
    this.writeSimpleText(get(data, "surname"), page, 35, position, 1);
    this.writeSimpleText(get(data, "firstName"), page, 35, position, 16);
    this.writeText(get(data, "countryOfBird"), page, position, 16);
    this.writeSimpleText(get(data, "currentNationally"), page, 3, position, 30);
    this.writeText(
      getDateFrom(get(data, "dateOfBirth")),
      page,
      position + 6,
      1
    );
    this.writeSimpleText(get(data, "occupation"), page, 20, position + 6, 25);
    if (data.addressInCZ) {
      this.writeSimpleText(
        `${data.addressInCZ.state || ""} ${data.addressInCZ.town || ""}` +
          ` ${data.addressInCZ.street || ""} ${data.addressInCZ.number || ""}`,
        page,
        34,
        position + 6,
        9
      );
    }
  }

  private setChildren(
    position: number,
    page: PDFPage,
    data: BaseUserDataDto
  ): void {
    this.writeSimpleText(
      `${get(data, "surname")} ${get(data, "firstName")}`,
      page,
      20,
      position,
      1
    );
    this.writeSimpleText(get(data, "currentNationally"), page, 3, position, 29);
    this.writeText(
      getDateFrom(get(data, "dateOfBirth")),
      page,
      position + 6.1,
      1
    );
    if (data.addressInCZ) {
      this.writeSimpleText(
        get(data, "addressInCZ.town"),
        page,
        34,
        position + 6.1,
        9
      );
      this.writeSimpleText(
        get(data, "addressInCZ.number"),
        page,
        4,
        position + 6.1,
        16
      );
      this.writeSimpleText(
        get(data, "occupation"),
        page,
        20,
        position + 6.1,
        26
      );
    }
  }
  private setSpouse(page: PDFPage, data: UserDataDto): void {
    //// change calibration !!!!
    this.config.calibrationYValue -= 4;
    //// change calibration !!!!
    this.writeText(get(data, "spouse.surname"), page, 20, 8);
    //// change calibration !!!!
    this.config.calibrationYValue -= 7;
    //// change calibration !!!!
    this.writeText(get(data, "spouse.otherNames"), page, 22, 0);
    //// change calibration !!!!
    this.config.calibrationYValue -= 5;
    //// change calibration !!!!
    this.writeText(get(data, "spouse.surnameAtBirth"), page, 24, 0);
    this.writeText(get(data, "spouse.firstName"), page, 25, 9);
    this.writeText(get(data, "spouse.currentNationally"), page, 26, 29);
    this.writeText(getDateFrom(get(data, "spouse.dateOfBirth")), page, 27, 24);
    //// change calibration !!!!
    this.config.calibrationYValue += 5;
    //// change calibration !!!!
    this.writeText(get(data, "spouse.countryOfBird"), page, 28, 29);
    //// change calibration !!!!
    this.config.calibrationYValue += 2;
    //// change calibration !!!!
    this.writeText(get(data, "spouse.placeOfBirth"), page, 29, 13);
    //// change calibration !!!!
    this.config.calibrationYValue += 2;
    //// change calibration !!!!
    this.writeText(get(data, "spouse.occupation"), page, 30, 10);
    this.writeText(get(data, "spouse.residence.street"), page, 32, 6);
    this.writeText(get(data, "spouse.residence.number"), page, 32, 27);
    this.writeText(get(data, "spouse.residence.town"), page, 33, 6);
    this.writeText(get(data, "spouse.residence.zip"), page, 33, 27);
    this.writeText(get(data, "spouse.residence.state"), page, 34, 29);
  }

  async generateDocument(data: UserDataDto) {
    const document = await PDFDocument.load(await this._file.arrayBuffer());
    document.registerFontkit(fontkit);

    const url = "/public/regular.otf";
    const ubuntuBytes = await fetch(url).then((res) => res.arrayBuffer());
    this._font = await document.embedFont(ubuntuBytes);
    let page = document.getPage(0);

    this.writeCase(
      [
        {
          value: FormTypes.permanentResidence,
          offsetX: 4.25,
          offsetY: -10.1,
        },
        {
          value: FormTypes.longTermResidence,
          offsetX: 3.35,
          offsetY: -9.07,
        },
        {
          value: FormTypes.extensionOfValidity,
          offsetX: -0.75,
          offsetY: -8.07,
        },
        {
          value: FormTypes.extensionOfValidityChange,
          offsetX: -0.8,
          offsetY: -6.6,
        },
      ],
      data.formType,
      page
    );
    this.writeText(get(data, "baseData.surname"), page);
    this.writeText(get(data, "baseData.otherNames"), page, 2, 0, true);
    this.writeText(get(data, "baseData.surnameAtBirth"), page, 5, 0, true);
    this.writeText(get(data, "baseData.firstName"), page, 7, 9);
    //// change calibration !!!!
    this.config.calibrationYValue -= 13;
    this.config.stepY *= 1.13;
    //// change calibration !!!!
    this.writeCase(
      [
        {
          value: Sex.male,
          offsetX: 16,
        },
        {
          value: Sex.female,
          offsetX: 28,
        },
      ],
      data.sex,
      page,
      8
    );

    this.writeText(getDateFrom(get(data, "baseData.dateOfBirth")), page, 9, 24);

    this.writeCase(
      [
        {
          value: MaritalStatus.single,
          offsetX: 31,
          offsetY: 10,
        },
        {
          value: MaritalStatus.married,
          offsetX: 9,
        },
        {
          value: MaritalStatus.divorced,
          offsetX: 20,
        },
        {
          value: MaritalStatus.widow,
          offsetX: 31,
        },
      ],
      data.maritalStatus,
      page,
      11
    );
    //// change calibration !!!!
    this.config.calibrationYValue += 3;
    //// change calibration !!!!
    this.writeText(get(data, "baseData.countryOfBird"), page, 12, 29);
    this.writeText(get(data, "baseData.placeOfBirth"), page, 13, 12);
    this.writeText(get(data, "baseData.currentNationally"), page, 14, 29);
    //// change calibration !!!!
    this.config.calibrationYValue -= 3;
    //// change calibration !!!!
    this.writeText(
      i18nGlobal.t(`enums.education.${data.baseData.education}`, Locales.cs),
      page,
      16,
      0
    );
    this.writeText(get(data, "baseData.occupation"), page, 17, 9);
    //// change calibration !!!!
    this.config.calibrationYValue += 10;
    //// change calibration !!!!
    // EMPLOYMENT BEFORE ARRIVAL TO THE CZECH REPUBLIC
    this.writeText(get(data, "employmentBeforeCz.employerName"), page, 19, 11);
    this.writeText(get(data, "employmentBeforeCz.position"), page, 20, 11);
    this.writeText(get(data, "employmentBeforeCz.address.street"), page, 21, 6);
    this.writeText(
      get(data, "employmentBeforeCz.address.number"),
      page,
      21,
      27
    );
    this.writeText(get(data, "employmentBeforeCz.address.town"), page, 22, 6);
    this.writeText(get(data, "employmentBeforeCz.address.zip"), page, 22, 27);
    // EMPLOYMENT BEFORE ARRIVAL TO THE CZECH REPUBLIC END
    //EMPLOYMENT AFTER ARRIVAL TO THE CZECH REPUBLIC
    this.writeText(get(data, "employmentInCzech.employerName"), page, 24, 11);
    this.writeText(get(data, "employmentInCzech.position"), page, 25, 11);
    this.writeText(get(data, "employmentInCzech.address.street"), page, 26, 6);
    this.writeText(get(data, "employmentInCzech.address.number"), page, 26, 27);
    this.writeText(get(data, "employmentInCzech.address.town"), page, 27, 6);
    this.writeText(get(data, "employmentInCzech.address.zip"), page, 27, 27);
    //EMPLOYMENT AFTER ARRIVAL TO THE CZECH REPUBLIC END
    //// change calibration !!!!
    this.config.calibrationYValue -= 5;
    //// change calibration !!!!
    this.writeText(get(data, "purposeOfStayInCR"), page, 29, 0);
    // page 2
    page = document.getPage(1);
    //// change calibration !!!!
    this.config.offsetYStart = 755;
    this.config.stepY = 16;
    //// change calibration !!!!
    // LAST RESIDENCE ABROAD
    this.writeText(get(data, "lastResidenceBeforeCz.street"), page, 1, 6);
    this.writeText(get(data, "lastResidenceBeforeCz.number"), page, 1, 27);
    this.writeText(get(data, "lastResidenceBeforeCz.town"), page, 2, 6);
    this.writeText(get(data, "lastResidenceBeforeCz.zip"), page, 2, 27);
    this.writeText(get(data, "lastResidenceBeforeCz.state"), page, 3, 29);
    // PREVIOUS STAY IN THE CZECH REPUBLIC LONGER THAN 3 MONTHS
    //// change calibration !!!!
    this.config.calibrationYValue -= 5;
    //// change calibration !!!!
    this.writeText(getDateFrom(get(data, "previousStayFrom")), page, 6, 12);
    this.writeText(getDateFrom(get(data, "previousStayTo")), page, 6, 24);
    //// change calibration !!!!
    this.config.calibrationYValue -= 5;
    //// change calibration !!!!
    this.writeText(get(data, "previousAndPlaceOfStay"), page, 8, 0);
    // RESIDENCE ADDRESS IN THE CZECH REPUBLIC
    //// change calibration !!!!
    this.config.calibrationYValue -= 6;
    //// change calibration !!!!
    this.writeText(get(data, "baseData.addressInCZ.street"), page, 10, 6);
    this.writeText(get(data, "baseData.addressInCZ.number"), page, 10, 27);
    this.writeText(get(data, "baseData.addressInCZ.town"), page, 11, 6);
    this.writeText(get(data, "baseData.addressInCZ.zip"), page, 11, 27);
    // DELIVERY ADDRESS IF DIFFERENT FROM RESIDENCE ADDRESS
    //// change calibration !!!!
    this.config.calibrationYValue += 5;
    //// change calibration !!!!
    this.writeText(
      get(data, "baseData.addressInCZDelivery.street"),
      page,
      13,
      6
    );
    this.writeText(
      get(data, "baseData.addressInCZDelivery.number"),
      page,
      13,
      27
    );
    this.writeText(get(data, "baseData.addressInCZDelivery.town"), page, 14, 6);
    this.writeText(get(data, "baseData.addressInCZDelivery.zip"), page, 14, 27);
    // DELIVERY ADDRESS IF DIFFERENT FROM RESIDENCE ADDRESS END
    this.writeText(getDateFrom(get(data, "dateOfArrivalToCz")), page, 15, 24);
    this.writeText(get(data, "numberTravelDocument"), page, 16, 21);
    //// change calibration !!!!
    this.config.calibrationYValue += 5;
    //// change calibration !!!!
    this.writeText(get(data, "countryTravelDocument"), page, 17, 29);
    this.writeText(
      getDateFrom(get(data, "validUntilTravelDocument")),
      page,
      18,
      24
    );
    this.setSpouse(page, data);
    //// change calibration !!!!
    this.config.stepY -= 3.5;
    //// change calibration !!!!
    if (data.children) {
      data.children.forEach((child, i) => {
        this.setChildren(47.7 + i, page, child);
      });
    }
    // page// page 3
    page = document.getPage(2);
    //// change calibration !!!!
    this.config.offsetYStart = 755;
    this.config.offsetXStart -= 10;
    this.config.stepY = 17.9;
    this.config.maxOffsetX += 1;
    this.config.calibrationYValue -= 9;
    //// change calibration !!!!
    this.setHusband(3, page, data.father);
    this.config.calibrationYValue += 5;
    this.setHusband(14, page, data.mother);
    //// change calibration !!!!
    this.config.calibrationYValue += 93;
    this.config.stepY -= 3.5;
    //// change calibration !!!!
    if (data.brothersAndSisters) {
      data.brothersAndSisters.forEach((brothersAndSisters, i) => {
        this.setSistersAndBrothers(26 + i, page, brothersAndSisters);
      });
    }

    this.writeSimpleText(get(data, "additionalInformation"), page, 75, 37);

    ////////////////
    return document.save();
  }
  private writeCase(
    cases: Array<{
      value: any;
      offsetX: number;
      offsetY?: number;
    }>,
    value: any,
    page: PDFPage,
    offsetY?: number
  ) {
    cases.forEach((_case) => {
      if (_case.value === value) {
        this.writeText("X", page, _case.offsetY ?? offsetY, _case.offsetX);
      }
    });
  }
  private writeText(
    text: string,
    page: PDFPage,
    offsetY?: number,
    offsetX?: number,
    multiline?: boolean
  ) {
    if (!text || text.trim().length === 0) {
      return;
    }
    if (multiline && text.length >= this.config.maxOffsetX) {
      this.writeText(
        text.substring(this.config.maxOffsetX + 1),
        page,
        (offsetY ?? 0) + 1
      );

      text = text.substring(0, this.config.maxOffsetX + 1);
    }
    Array.from(text.trim().toUpperCase()).forEach((letter, i) => {
      page.drawText(letter, {
        x: this.config.offsetXStart + this.config.stepX * ((offsetX ?? 0) + i),
        y:
          this.config.offsetYStart -
          this.config.calibrationYValue -
          this.config.stepY * (offsetY ?? 0),
        size: this.config.fontSize,
        font: this._font,
      });
    });
  }
  private writeSimpleText(
    text: string,
    page: PDFPage,
    lengthX: number,
    offsetY?: number,
    offsetX?: number
  ) {
    if (!text || text.trim().length === 0) {
      return;
    }
    const splitText = text.trim().match(new RegExp(`.{1,${lengthX}}`, "g"));
    if (!splitText) {
      return;
    }
    page.drawText(splitText.join("\n"), {
      x: this.config.offsetXStart + this.config.stepX * (offsetX ?? 0),
      y:
        this.config.offsetYStart -
        this.config.calibrationYValue -
        this.config.stepY * (offsetY ?? 0),
      lineHeight: this.config.fontSize,
      size: this.config.fontSize,
      font: this._font,
    });
  }
}
