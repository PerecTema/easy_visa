export const getDateFrom = (date: string | Date | null): string => {
  if (!date) {
    return "";
  }
  const _date = new Date(date);
  return `${twoNumberDate(_date.getDate())}${twoNumberDate(
    _date.getMonth() + 1
  )}${_date.getFullYear()}`;
};
export const twoNumberDate = (number: number): string => {
  return ("0" + number).slice(-2);
};
