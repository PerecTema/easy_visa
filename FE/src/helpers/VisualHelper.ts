import { onUnmounted, type Ref } from "vue";
import { round } from "lodash";
import router from "@/router";

export const scrollIntoView = (element: HTMLElement): void => {
  window.scroll({ top: element.offsetTop, behavior: "smooth" });
};

export const watchWhenElementIsInViewport = (
  componentRef: Ref<HTMLElement | null>
) => {
  function isInViewport(element: { getBoundingClientRect: () => DOMRect }) {
    const rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      round(rect.bottom - 1) <=
        round(window.innerHeight || document.documentElement.clientHeight)
    );
  }
  const callback = async () => {
    if (componentRef.value && isInViewport(componentRef.value)) {
      const fullPath = router.currentRoute.value.fullPath;
      const hash = router.currentRoute.value.hash;
      console.log(hash);
      if (hash) {
        await router.replace(
          fullPath.replace(hash, `#${componentRef.value.id}`)
        );
      } else {
        await router.replace(`${fullPath}#${componentRef.value.id}`);
      }
    }
  };
  document.addEventListener("scroll", callback);
  document.addEventListener("resize", callback);
  onUnmounted(() => {
    document.removeEventListener("scroll", callback);
    document.removeEventListener("resize", callback);
  });
  return componentRef;
};
