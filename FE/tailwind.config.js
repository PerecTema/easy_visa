module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      minHeight: {
        "90-vh": "90vh",
      },
    },
  },
  plugins: [require("tw-elements/dist/plugin")],
};
