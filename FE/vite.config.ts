import { fileURLToPath, URL } from "url";

import { defineConfig, loadEnv } from "vite";
import EnvironmentPlugin from "vite-plugin-environment";
import vue from "@vitejs/plugin-vue";

process.env = { ...process.env, ...loadEnv("", process.cwd(), "") };
const port = process.env.FE_PORT;

export default defineConfig({
  server: {
    port: port && !isNaN(parseInt(port)) ? parseInt(port) : undefined,
    host: process.env.FE_HOST,
  },
  plugins: [
    vue(),
    EnvironmentPlugin([
      "BE_HOST",
      "NODE_ENV",
      "BE_PORT",
      "BE_HOST",
      "API_VERSION",
      "FE_PORT",
      "FE_HOST",
    ]),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
