FROM node:18
RUN npm install -g npm@8.14.0
USER root

RUN apt-get update && apt-get install -y libcap2-bin
RUN setcap cap_net_bind_service=+ep `readlink -f \`which node\``

USER node