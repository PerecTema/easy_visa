import type AddressDto from "./AddressDto";

export default interface EmploymentDto {
  employerName: string;
  position: string;
  address: AddressDto;

}
