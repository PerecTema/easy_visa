export default interface NewUserDto {
  email: string,
  password: string,
  isFirm: boolean,
  tenant: string,
}
