import {CountryCodes, Education} from "../Enums/DataBaseEnums";
import type AddressDto from "./AddressDto";


export default interface BaseUserDataDto {
  surname: string;
  surnameOther: string;
  surnameAtBirth: string;
  firstName: string;
  firstNameAtBirth: string;
  dateOfBirth: string;
  otherNames: string;
  placeOfBirth: string;
  occupation: string;
  education: Education;
  countryOfBird: CountryCodes;
  originalNationally: CountryCodes;
  currentNationally: CountryCodes;
  residence: AddressDto;
  addressInCZ: AddressDto;
  addressInCZDelivery: AddressDto;

}
