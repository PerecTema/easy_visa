import type BaseUserDataDto from "./BaseUserDataDto";
import type EmploymentDto from "./EmploymentDto";
import type AddressDto from "./AddressDto";
import type {CountryCodes, Education, MaritalStatus, Sex, TypeOfTravelDocument, FormTypes} from "../Enums/DataBaseEnums";

export default interface UserDataDto {
  id: string;
  created: Date;
  updated: Date;
  deletedAt: Date;
  education: Education;
  formType: FormTypes;
  baseData: BaseUserDataDto;
  father: BaseUserDataDto;
  spouse: BaseUserDataDto;
  maritalStatus: MaritalStatus;
  sex: Sex;
  mother: BaseUserDataDto;
  typeOfTravelDocument: TypeOfTravelDocument;
  numberTravelDocument: string;
  previousAndPlaceOfStay: string;
  issuedBy: CountryCodes;
  dateOfIssue: string;
  validUntilTravelDocument: string;
  employmentBeforeCz: EmploymentDto;
  employmentInCzech: EmploymentDto;
  lastResidenceBeforeCz: AddressDto;
  previousStayFrom: Date;
  previousStayTo: Date;
  purposeOfStayInCR: string;
  countryTravelDocument: CountryCodes;
  dateOfArrivalToCz: Date;
  children: BaseUserDataDto[];
  brothersAndSisters: BaseUserDataDto[];
  additionalInformation: string;
}
