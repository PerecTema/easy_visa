export interface ITokens {
  accessToken: string,
  resetToken: string,
}

export interface IJwtPayload {
  exp: number
  id: string,
  email: string
}
