import type UserDataDto from "./UserDataDto";

export default interface UserDto {
  id: string,
  created: Date,
  updated: Date,
  deletedAt: Date,
  userData: UserDataDto,
  email: string,
  token: string,
  isFirm: boolean,
  parent: UserDto,
}
