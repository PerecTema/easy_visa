import type {CountryCodes} from "../Enums/DataBaseEnums";


export default interface AddressDto  {
  street: string;
  number: string;
  town: string;
  zip: string;
  tel: string;
  state: CountryCodes;

}
