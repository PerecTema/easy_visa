export enum RoutUserEnums {
  user = 'user',
  userCreate = 'userCreate',
  putUserData = 'putUserData',
  getUserData = 'getUserData',
  userLogin = 'login',
  userGet = 'get',
  getUsersList = 'getUsers',
  addUser = 'addUser',
}
export enum RoutFilesEnums {
  files = 'files'
}
