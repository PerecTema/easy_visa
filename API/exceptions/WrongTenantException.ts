import {ErrorCodes} from "#exceptions/ErrorCodes";

import {Exception} from "./Exception";

export class WrongTenantException extends Exception{
	constructor(tenantId: string) {
		super(`Tenant with ID [${tenantId}] not found.`, ErrorCodes.tenantNotFound);
	}
}
