import {ErrorCodes} from "./ErrorCodes";
import {Exception} from "./Exception";

export class MissingParameterException extends Exception{

    constructor(missingParameter: string, name: ErrorCodes) {
        super(`Missing [${missingParameter}]`, name);
    }
}
