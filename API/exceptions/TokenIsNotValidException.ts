import {ErrorCodes} from "./ErrorCodes";
import {Exception} from "./Exception";

export class TokenIsNotValidException extends Exception{

    constructor(message?: string) {
        super(message || "Token is not valid.", ErrorCodes.tokenIsNotValid);
    }
}
