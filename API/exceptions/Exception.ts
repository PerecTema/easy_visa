import type {ErrorCodes} from "./ErrorCodes";

export declare type ExceptionType = { message: string; code: string }

export class Exception extends Error{
	message: string;
	code: ErrorCodes;

	constructor(message: string, name: ErrorCodes) {
		super(message);
		this.message = message;
		this.code = name;
	}

	toJson(): ExceptionType {
		return {
			message: this.message,
			code: this.code,
		}
	}

	stringify() {
		return JSON.stringify(this.toJson());
	}
}
