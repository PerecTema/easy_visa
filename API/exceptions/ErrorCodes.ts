export enum ErrorCodes {
  userNotFound = 'userNotFound',
  userDataNotFound = 'userDataNotFound',
  userChangePermission = 'userChangePermission',
  userWithEmailExist = 'userWithEmailExist',
  sessionNotFound = 'sessionNotFound',
  headerAuthorizationMissing = 'headerAuthorizationMissing',
  tokenIsNotValid = 'tokenIsNotValid',
  headerTenantMissing = 'headerTenantMissing',
  headerProviderMissing = 'headerProviderMissing',
  tenantNotFound = 'tenantNotFound',
  rowNotFound = 'rowNotFound',
  registrationAdmin = 'registrationAdmin',
  touchUser = "touchUser",
  fileNotFound = "fileNotFound"
}
